/**

CHECK PAGE TO ACTIVEATE MENU ITEMS IN MIDDLEBAR

HOME HREF = "/"

NOT TESTED YET WITH SUBMENUS

ADD THIS FILE TO JS/COMPONENENTS FOLDER, INCLUDE IN FOOTER 

<script src="/css/js/components/active-menu-items.js"></script>

**/


(function($) {

    var url = window.location.pathname.substr(1); // returns the full URL
    var middleMenuLeft = $('.middlebar-menu-left');
    var middleMenuRight = $('.middlebar-menu-right');

    if (typeof url !== 'undefined') {
        if (url !== '') {
            var middleMenuLeftItems = $(middleMenuLeft).find("a[href^='/" + url + "']");
            var middleMenuRightItems = $(middleMenuRight).find("a[href^='/" + url + "']");
        } else {
            var middleMenuLeftItems = $(middleMenuLeft).find("a[href='/']");
            var middleMenuRightItems = $(middleMenuRight).find("a[href='/']");
        }
        middleMenuLeftItems.each(function() {
            $(this).addClass('active');
        });
        middleMenuRightItems.each(function() {
            $(this).addClass('active');
        });

    } else {


    }


})(jQuery);
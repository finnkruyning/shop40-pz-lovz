$(function() {

    var pathname = window.location.pathname;


    if (!$("#headerlogin").length) {
        $('.topbar-menu-right').find('#navbar-login').remove();
    }
    if (!$("#headerlogout").length) {
        $('.topbar-menu-right').find('#navbar-logout').remove();
    }
    $('#pagecontent').find('.visual').insertAfter($('.navbar')); // set visual to visual content in header

    if ($('#body_design')) {
        var backLink = $('.crumb-mobile');

        $('#crumbs').append(backLink);
    }


    // SIDEBAR MENU COLLAPSES

    $('.sidebar_widget').on('click', ' ul.list > li > a', function(e) {
        e.preventDefault();
        $(this).parent('li').siblings().removeClass('active');
        $(this).parent('li').toggleClass('active');
    });

    $('.sidebar_widget  ul.list > li:nth-child(2)').addClass('active');


    $('#sidebar ul li ul li').each(function() {
        var hre = $(this).find('a').attr('href');

        if (hre == pathname) {
            $(this).addClass('active');
        }
    });

        // var url = window.location.pathname;
        // console.log(url);
        // // OPEN MOBILE MENU
        // if (typeof(url) != 'undefined') {
        //     var urlPart = url.split("/")[1];
        //     // if(urlPart == 'trouwkaarten'){
        //     //     if (url.search('save-the-date') != -1){
        //     //         urlPart = 'trouwkaarten/save-the-date'
        //     //     }
        //     // }
        //     console.log(urlPart);
        //     var thisMenuItem = $('.middlebar-menu-right').find('a[href^="/' + urlPart + '"]');
        //     if (typeof(thisMenuItem) != 'undefined') {
        //         thisMenuItem.first().siblings('ul').css({ 'display': 'flex' });
        //         thisMenuItem.first().parent('li').addClass('open');
        //     }

        // }




});
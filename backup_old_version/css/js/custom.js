jQuery(function(){
	jQuery('#slides .bxslider').bxSlider({
	  auto: true
	});

	jQuery('div#menu ul').superfish({
		delay:       300,                            // one second delay on mouseout
		animation:   {'marginLeft':'0px',opacity:'show',height:'show'},  // fade-in and slide-down animation
		speed:       'fast',                          // faster animation speed
		autoArrows:  true,                           // disable generation of arrow mark-up
		onBeforeShow:      function(){ this.css('marginLeft','20px'); },
		dropShadows: false                            // disable drop shadows
	});

	var $str = jQuery("#footer .fnav").html();
	jQuery("body").append("<div class='fnav'>" + $str + "</div>");

	jQuery("#body_account #pagecontent .button:eq(2)").hide();
	jQuery("#body_account #pagecontent .button:eq(3)").hide();

	jQuery('.lightbox').click(function(event){
		event.preventDefault();
		jQuery('body').append('<div id="bg-lightbox"></div>').show('slow');
		jQuery('body').append('<div id="lightbox"><script>jQuery(document).ready(function(){jQuery("#bg-lightbox").click(function(){jQuery(this).hide("slow");jQuery(this).remove();jQuery("#lightbox").hide("slow");jQuery("#lightbox").remove();});jQuery("#lightbox .close").click(function(){jQuery("#bg-lightbox").hide("slow");jQuery("#bg-lightbox").remove();jQuery("#lightbox").hide("slow");jQuery("#lightbox").remove();});});</script><span class="close">Close</span><iframe src="/css/js/calculator/calculator.html"></iframe></div>').show('slow');
	});

	if (jQuery('#body_design').length >0) {
		jQuery('#voor_preview').parent('div').before('<div class="proefdruk">Proefdruk €1,- & gratis verzending</div>');
	}

	if(jQuery('#body_keuze').length){
		//jQuery( "ul li:nth-child(2)" )
	}

});
